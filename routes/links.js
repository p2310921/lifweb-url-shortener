import process from "node:process";
import Joi from "joi";
import Boom from "@hapi/boom";

export default {
  name: "links",
  version: process.env.npm_package_version,
  register: async function linksHandler(server, _options) {
    const { db } = server.app;
    const { prefix } = server.realm.modifiers.route;

    server.route({
      method: "GET",
      path: "/",
      handler: async (request, h) => {
        return await db.getAllLinksStats();
      },
    });

    server.route({
      method: "GET",
      path: "/{short}",
      handler: async (request, h) => {
        const { short } = request.params;
        const link = await server.app.db.getLinkByShort(short);
        if (link === undefined) {
          throw Boom.notFound(`Shortened link ${short} not found`);
        }
        server.log("debug", `Redirecting to ${link.long}`);
        return h.redirect(link.long);
      },
    });

    server.route({
      method: "GET",
      path: "/{short}/status",
      handler: async (request, h) => {
        throw Boom.notImplemented("Not implemented");
      },
    });

    server.route({
      method: "DELETE",
      path: "/{short}",
      handler: async (request, h) => {
        throw Boom.notImplemented("Not implemented");
      },
    });

    server.route({
      method: "POST",
      path: "/",
      handler: async (request, h) => {
        const { uri } = request.payload;
        const link = await db.createLink(uri);
        return h.response({ ...link, uri: `${server.info.uri}${prefix}/${link.short}` }).code(201);
      },
      options: {
        validate: {
          payload: Joi.object({
            uri: Joi.string().uri().example("http://perdu.com"),
          }),
        },
      },
    });
  },
};
